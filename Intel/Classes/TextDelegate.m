//
//  TextDelegate.m
//  File Type & Creator Editor
//
//  Created by Petros on 8/30/17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//

#import "TextDelegate.h"
#import "AppDelegate.h"

@implementation TextDelegate

- (void)textDidChange:(NSNotification *)notification {
	AppDelegate *appDelegate = (AppDelegate *) [NSApp delegate];
    NSText *editor = [self currentEditor];
    NSRange range = [editor selectedRange];
    NSUInteger loc = range.location;
    NSMutableString *currentText = [[self stringValue] mutableCopy];
    if ([currentText length] > 4) {
        NSBeep();
		[currentText deleteCharactersInRange:NSMakeRange(loc - 1, 1)];
        [self setStringValue:currentText];
        [[self currentEditor] setSelectedRange:NSMakeRange(loc - 1, 0)];
    } else {
        [appDelegate resetMainWindowPopUpMenu];
    }
}

@end


