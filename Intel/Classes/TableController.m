//
//  TableController.m
//  File Type & Creator Editor
//
//  Created by Petros on 8/30/17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//


#import "TableController.h"


@implementation TableController


#pragma mark Getters and Setters

- (NSArray *)selectedRows {
    NSIndexSet *theSelectedRowIndexSet = [presetSheetTableView selectedRowIndexes];
    if (theSelectedRowIndexSet == nil) {
        return nil;
    }
    selectedRows = [[NSMutableArray alloc] init];
    NSUInteger rowNumber = [theSelectedRowIndexSet firstIndex];
    do {
        [selectedRows addObject:[NSNumber numberWithUnsignedInteger:rowNumber]];
        rowNumber = [theSelectedRowIndexSet indexGreaterThanIndex:rowNumber];
    } while (rowNumber < NSIntegerMax);
    return [[selectedRows retain] autorelease];
}


#pragma mark IBActions

- (IBAction)addNewTableRow:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    [self forceEndEditing];
    NSMutableArray *presetList = [[appDelegate presetList] mutableCopy];
    NSUInteger countOfRows = [presetList count];
    [presetList addObject:[NSMutableArray arrayWithObjects:@"New Setting", @"????", @"????", nil]];
    [appDelegate setPresetList:presetList];
    if (countOfRows > 11) {
        [[presetSheetScrollView contentView] scrollToPoint:NSMakePoint(0, 17 * countOfRows)];
    }
    [presetSheetTableView reloadData];
    NSUInteger rowToSelect = [presetList count] - 1;
    NSIndexSet *indexset = [NSIndexSet indexSetWithIndex:rowToSelect];
    [presetSheetTableView selectRowIndexes:indexset byExtendingSelection:NO];
}

- (IBAction)deleteTableRows:(id)sender {
    int i;
    NSArray *currentSelection = [self selectedRows];
    NSIndexSet *selected = [presetSheetTableView selectedRowIndexes];
    NSUInteger numberOfRowsToDelete = [currentSelection count];
    if (numberOfRowsToDelete == 0) {
        return;
    }
    [self forceEndEditing];
    NSMutableArray *presetList = [[appDelegate presetList] mutableCopy];
    for (i = 0; i < [currentSelection count]; i++) {
        [presetList replaceObjectAtIndex:[[currentSelection objectAtIndex:i] intValue] withObject:[NSNull null]];
    }
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    if ([appDelegate OSVersion] > 6) {
        [presetSheetTableView beginUpdates];
        [presetSheetTableView removeRowsAtIndexes:selected withAnimation:0x31];
        [presetList removeObjectIdenticalTo:[NSNull null]];
        [appDelegate setPresetList:presetList];
        [presetSheetTableView endUpdates];
    } else {
        [presetList removeObjectIdenticalTo:[NSNull null]];
        [appDelegate setPresetList:presetList];
        [presetSheetTableView reloadData]; 
    }
    [presetSheetTableView deselectAll:nil];
    [presetSheetTableView noteNumberOfRowsChanged];
}

- (IBAction)wrapUpTableEditing:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    [self forceEndEditing];
    [appDelegate removepresetSheetOnOK];
}

- (IBAction)cancelTableEditing:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    [self forceEndEditing];
    [appDelegate removepresetSheetOnCancel];
}


#pragma mark Table Controller/DataSource Methods

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
	return [[appDelegate presetList] count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    NSArray *presetList = [appDelegate presetList];
    NSArray *currentlyStrobed;
    NSString *currentRowAndCol = @"";
    currentlyStrobed = [presetList objectAtIndex:row];
    if ([[tableColumn identifier] isEqualToString:@"name"]) {
        currentlyStrobed = [presetList objectAtIndex:row];
        currentRowAndCol = [currentlyStrobed objectAtIndex:0];
    }
    if ([[tableColumn identifier] isEqualToString:@"type"]) {
        currentlyStrobed = [presetList objectAtIndex:row];
        currentRowAndCol = [currentlyStrobed objectAtIndex:1];
    }
    if ([[tableColumn identifier] isEqualToString:@"creator"]) {
        currentlyStrobed = [presetList objectAtIndex:row];
        currentRowAndCol = [currentlyStrobed objectAtIndex:2];
    }
    return currentRowAndCol;
}

- (BOOL)tableView:(NSTableView *)tableView writeRowsWithIndexes:(NSIndexSet *)rowindexes toPasteboard:(NSPasteboard *)pboard {
    NSData *rowData = [NSArchiver archivedDataWithRootObject:rowindexes];
    [pboard declareTypes:[NSArray arrayWithObjects:PrivateTableViewDataType, nil] owner:self];
    [pboard setData:rowData forType:PrivateTableViewDataType];
    return YES;
}

- (NSDragOperation)tableView:(NSTableView *)tableView validateDrop:(id<NSDraggingInfo>)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)dropOperation {
    [tableView setDropRow:row dropOperation:NSTableViewDropAbove];
    return NSDragOperationMove;
}

- (BOOL)tableView:(NSTableView *)tableView acceptDrop:(id<NSDraggingInfo>)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)dropOperation {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    int i;
    NSUInteger dropCount = 0;
    NSMutableArray *presetList = [[appDelegate presetList] mutableCopy];
    NSPasteboard *thePasteBoard = [info draggingPasteboard];
    NSData *rowData = [thePasteBoard dataForType:PrivateTableViewDataType];
    NSIndexSet *rowIndexes = [NSUnarchiver unarchiveObjectWithData:rowData];
    NSMutableArray *keepArray = [NSMutableArray arrayWithCapacity:[rowIndexes count]];
    NSUInteger rowNumber = [rowIndexes firstIndex];
    do {
        [keepArray addObject:[presetList objectAtIndex:rowNumber]];
        [presetList replaceObjectAtIndex:rowNumber withObject:[NSNull null]];
        if (rowNumber < row) {
            dropCount++;
        }
        rowNumber = [rowIndexes indexGreaterThanIndex:rowNumber];
    } while (rowNumber < NSIntegerMax);
    NSUInteger newDropRow = row - dropCount;
    if ([appDelegate OSVersion] > 6) {
        [presetSheetTableView beginUpdates];
        NSIndexSet *insertIdxs = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(newDropRow, [rowIndexes count])];
        [presetSheetTableView removeRowsAtIndexes:rowIndexes withAnimation:0x31];
        [presetSheetTableView insertRowsAtIndexes:insertIdxs withAnimation:0x32];
        [presetList removeObjectIdenticalTo:[NSNull null]];
        [presetList replaceObjectsInRange:NSMakeRange(newDropRow, 0) withObjectsFromArray:keepArray];
        [appDelegate setPresetList:presetList];
        [presetSheetTableView endUpdates];
    } else {
        [presetList removeObjectIdenticalTo:[NSNull null]];
        [presetList replaceObjectsInRange:NSMakeRange(newDropRow, 0) withObjectsFromArray:keepArray];
        [appDelegate setPresetList:presetList];
        [presetSheetTableView reloadData];
    }
    NSInteger selRow = row;
    NSInteger rowCount = [rowIndexes count];
    NSIndexSet *indexset = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(selRow - dropCount, rowCount)];
    [presetSheetTableView selectRowIndexes:indexset byExtendingSelection:NO];
    return YES;
}


#pragma mark Editing Control Methods

- (void)controlTextDidBeginEditing:(NSNotification *)notification {
    NSDictionary *userdict = [notification userInfo];
    NSText *fieldEditor = [userdict objectForKey:@"NSFieldEditor"];
    originalValue = [fieldEditor string];

}

- (void)controlTextDidChange:(NSNotification *)notification {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    NSObject *object = [notification object];
    NSDictionary *userdict = [notification userInfo];
    NSText *fieldEditor = [userdict objectForKey:@"NSFieldEditor"];
	NSRange fieldEditorSelectedRange = [fieldEditor selectedRange];
    NSUInteger insertPosition = fieldEditorSelectedRange.location;
    NSString *currentText = [[[NSString alloc] initWithString:[fieldEditor string]] autorelease];
    NSInteger currentColumn = [object editedColumn];
    if (currentColumn == 0) {
        return;
    }
    if ([currentText length] > 4) {
        NSBeep();
        currentText = [appDelegate deleteCharFromString:currentText atLocation:insertPosition - 1];
        [fieldEditor setString:currentText];
        [fieldEditor setSelectedRange:NSMakeRange(insertPosition - 1, 0)];
    }
}

- (BOOL)control:(NSControl *)control textShouldEndEditing:(NSText *)fieldEditor {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    NSMutableArray *presetList = [[appDelegate presetList] mutableCopy];
    NSUInteger currentColumn = [control editedColumn];
    NSUInteger currentRow = [control editedRow];
    NSString *currentText = [[[NSString alloc] initWithString:[fieldEditor string]] autorelease];
    if (currentColumn > 0 && [currentText length] < 4) {
        currentText = [appDelegate padTypeOrCreatorToFullWidth:currentText];
    }
    NSMutableArray *rowRepresentation = [[presetList objectAtIndex:currentRow] mutableCopy];
    [rowRepresentation replaceObjectAtIndex:currentColumn withObject:(NSString *)currentText];
    [presetList replaceObjectAtIndex:currentRow withObject:rowRepresentation];
    [appDelegate setPresetList:presetList];
    return YES;
}

- (void)forceEndEditing {
    if ([presetSheetTableView currentEditor]) {
		if (originalValue) {
			[[presetSheetTableView currentEditor] setString:originalValue];
		}
    }
	[presetSheet performSelector:@selector(makeFirstResponder:) withObject:presetSheetTableView afterDelay:0.0];
}

@end
