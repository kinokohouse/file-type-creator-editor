//
//  TableController.h
//  File Type & Creator Editor
//
//  Created by Petros on 8/30/17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//


#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "AppDelegate.h"


#pragma mark Interface

@interface TableController : NSTableView {
    IBOutlet NSWindow *presetSheet;
    IBOutlet NSTableView *presetSheetTableView;
    IBOutlet NSScrollView *presetSheetScrollView;
    IBOutlet NSObject *appDelegate;

    NSString *originalValue;
    NSMutableArray *selectedRows;
}


#pragma mark Getters and Setters

- (NSArray *)selectedRows;


#pragma mark IBActions

- (IBAction)addNewTableRow:(id)sender;
- (IBAction)deleteTableRows:(id)sender;
- (IBAction)wrapUpTableEditing:(id)sender;
- (IBAction)cancelTableEditing:(id)sender;


#pragma mark Table Controller/DataSource Methods

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView;
- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row;
- (BOOL)tableView:(NSTableView *)tableView writeRows:(NSArray *)rows toPasteboard:(NSPasteboard *)pboard;
- (NSDragOperation)tableView:(NSTableView *)tableView validateDrop:(id<NSDraggingInfo>)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)dropOperation;
- (BOOL)tableView:(NSTableView *)tableView acceptDrop:(id<NSDraggingInfo>)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)dropOperation;


#pragma mark Editing Control Methods

- (void)controlTextDidBeginEditing:(NSNotification *)notification;
- (void)controlTextDidChange:(NSNotification *)notification;
- (BOOL)control:(NSControl *)control textShouldEndEditing:(NSText *)fieldEditor;
- (void)forceEndEditing;

@end
