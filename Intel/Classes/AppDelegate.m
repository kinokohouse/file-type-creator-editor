//
//  AppDelegate.m
//  File Type & Creator Editor
//
//  Created by Petros on 8/30/17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//

#import "AppDelegate.h"

NSString *const PrivateTableViewDataType = @"FTCEPrivateTableViewDataType";

@implementation AppDelegate


#pragma mark Getters and Setters

- (NSString *)zeroString {
    return [[zeroString retain] autorelease];
}

- (NSArray *)presetList {
    return [[presetList retain] autorelease];
}

- (void)setPresetList:(NSMutableArray *)array {
    [presetList autorelease];
    presetList = [array retain];
}

- (NSString *)passedURL {
    return [[passedURL retain] autorelease];
}

- (void)setPassedURL:(NSString *)passedPath {
    [passedURL autorelease];
    passedURL = [passedPath retain];
}

- (NSArray *)passedURLList {
    return [[passedURLList retain] autorelease];
}

- (void)setPassedURLList:(NSArray *)urlList {
    [passedURLList autorelease];
    passedURLList = [urlList retain];
}

- (NSArray *)presetListSafetyCopy {
	return [[presetListSafetyCopy retain] autorelease];
}

- (void)setPresetListSafetyCopy:(NSArray *)array {
	[presetListSafetyCopy autorelease];
    presetListSafetyCopy = [array retain];
}

- (NSString *)fileName {
	return [[fileName retain] autorelease];
}

- (void)setFileName:(NSString *)name {
    [fileName autorelease];
    fileName = [name retain];
}

- (NSString *)fileNameWithFullPath {
	return [[fileNameWithFullPath retain] autorelease];
}

- (void)setFileNameWithFullPath:(NSString *)path {
	[fileNameWithFullPath autorelease];
	fileNameWithFullPath = [path retain];
}

- (NSString *)previousType {
	return [[previousType retain] autorelease];
}

- (void)setPreviousType:(NSString *)prevtype {
	[previousType autorelease];
	previousType = [prevtype retain];
}

- (NSString *)previousCreator {
	return [[previousCreator retain] autorelease];
}

- (void)setPreviousCreator:(NSString *)prevcreator {
	[previousCreator autorelease];
	previousCreator = [prevcreator retain];
}

- (NSUInteger)OSVersion {
    return OSVersion;
}


#pragma mark IBActions

- (IBAction)showpreferencesSheet:(id)sender {
	[self disableFileMenu];
	[NSApp beginSheet:preferencesSheet modalForWindow:mainWindow modalDelegate:self didEndSelector:@selector(preferencesSheetRemoved) contextInfo:nil];
}

- (IBAction)updatePreferences:(id)sender {
    [self setPreferences];
	[NSApp endSheet:preferencesSheet];
	[preferencesSheet orderOut:self];
	[self enableFileMenu];
}

- (IBAction)cancelPreferences:(id)sender {
	[preferencesSheetBatchDropCheckBox setState:preferencesBatchDrop];
	[preferencesSheetRememberPositionCheckBox setState:preferencesRememberPosition];
	[NSApp endSheet:preferencesSheet];
	[preferencesSheet orderOut:self];
	[self enableFileMenu];
}

- (IBAction)showHelpWindow:(id)sender {
    [helpWindow makeKeyAndOrderFront:self];
}

- (IBAction)getFileToLoad:(id)sender {
    NSOpenPanel *filePanel = [NSOpenPanel openPanel];
    [filePanel setCanChooseFiles: YES];
    [filePanel setCanChooseDirectories: NO];
    [filePanel setAllowsMultipleSelection:NO];
    NSString *openDirectory = nil;
    if (firstOpen) {
        openDirectory = NSHomeDirectory();
    }
    NSInteger buttonReturned = [filePanel runModalForDirectory:openDirectory file:nil types:nil];
    if (buttonReturned == NSCancelButton) {
         return;
    }
    if ([filePanel filenames] != nil) {
        [self setFileNameWithFullPath:[[filePanel filenames] objectAtIndex:0]];
        [self setFileName:[[self fileNameWithFullPath] lastPathComponent]];
    } else {
        NSLog(@"Something went wrong when picking a file - no details");
    }
    [self fileWasPickedWithOpenButton];
}

- (IBAction)clearCurrentlyLoadedFile:(id)sender {
    NSImage *newImage = [[[NSImage alloc] initWithSize:NSMakeSize(32, 32)] autorelease];
    [mainWindowImageWell setImage:newImage];
    [mainWindowFileLabel setStringValue:@"No file selected."];
    [mainWindowTypeIndicator setStringValue:@"????"];
    [mainWindowCreatorIndicator setStringValue:@"????"];
    [menuItemSet setEnabled:NO];
    [mainWindowSetButton setEnabled:NO];
    [mainWindowSetButton setToolTip:@"Sets the selected file above to the file type and creator on the left. Currently not available because no file is currently selected."];
    [menuItemReset setEnabled:NO];
    [mainWindowResetButton setEnabled:NO];
    [mainWindowResetButton setToolTip:@"Reverts to the file type and creator values as they were before editing. Currently not available because no file is currently selected."];
    [menuItemClear setEnabled:NO];
    [mainWindowClearButton setEnabled:NO];
    [mainWindowClearButton setToolTip:@"Click here to clear the current selection. Currently not available because no file is currently selected."];
}

- (IBAction)restorePreviousTypeAndCreator:(id)sender {
    NSString *typeForRestore;
    NSString *creatorForRestore;
    if ([self previousType] == zeroString) {
        typeForRestore = @"";
    } else {
        typeForRestore = [self previousType];
    }
    if ([self previousCreator] == zeroString) {
        creatorForRestore = @"";
    } else {
        creatorForRestore = [self previousCreator];
    }
    [mainWindowTypeTextField setStringValue:typeForRestore];
    [mainWindowCreatorTextField setStringValue:creatorForRestore];
    [self enableButtonsAndSetFirstResponder];
}

- (IBAction)grabFileInfo:(id)sender {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSWorkspace *workSpace = [NSWorkspace sharedWorkspace];
    NSOpenPanel *filePanel = [NSOpenPanel openPanel];
    OSType grabbedOSType;
    OSType grabbedOSCreator;
    NSString *grabbedQuotedType;
    NSString *grabbedQuotedCreator;
    NSString *grabbedType;
    NSString *grabbedCreator;
    [filePanel setCanChooseFiles: YES];
    [filePanel setCanChooseDirectories: NO];
    [filePanel setAllowsMultipleSelection:NO];
    NSString *openDirectory = nil;
    if (firstOpen) {
        openDirectory = NSHomeDirectory();
    }
    NSInteger buttonReturned = [filePanel runModalForDirectory:openDirectory file:nil types:nil];
    if (buttonReturned == NSCancelButton) {
        return;
    }
    if ([filePanel filenames] == nil) {
        NSLog(@"Something went wrong when picking a file - no details");
        return;
    } else {
        NSString *grabName = [[filePanel filenames] objectAtIndex:0];
        BOOL result = [workSpace isFilePackageAtPath:grabName];
        if (!result) {
            NSDictionary *attribDict = [fileManager fileAttributesAtPath:grabName traverseLink:NO];
            grabbedOSType = [[attribDict objectForKey:@"NSFileHFSTypeCode"] unsignedIntValue];
            grabbedOSCreator = [[attribDict objectForKey:@"NSFileHFSCreatorCode"] unsignedIntValue];
            grabbedQuotedType = NSFileTypeForHFSTypeCode(grabbedOSType);
            grabbedQuotedCreator = NSFileTypeForHFSTypeCode(grabbedOSCreator);
            grabbedType = [self trimmedString:grabbedQuotedType];
            grabbedCreator = [self trimmedString:grabbedQuotedCreator];
            if (grabbedType == zeroString) {
                grabbedType = @"";
            }
            if (grabbedCreator == zeroString) {
                grabbedCreator = @"";
            }
            [mainWindowTypeTextField setStringValue:grabbedType];
            [mainWindowCreatorTextField setStringValue:grabbedCreator];
            [self setFirstResponderAfterGrab];
        } else {
            NSString *infoName = [NSString stringWithFormat:@"%@/Contents/Info.plist", grabName];
            BOOL fileExists = [fileManager fileExistsAtPath:infoName];
            if (!fileExists) {
                [mainWindowTypeTextField setStringValue:@""];
                [mainWindowCreatorTextField setStringValue:@""];
            } else {
                NSDictionary *propList = [NSDictionary dictionaryWithContentsOfFile:infoName];
                grabbedType = [propList objectForKey:@"CFBundlePackageType"];
                grabbedCreator = [propList objectForKey:@"CFBundleSignature"];
                if (grabbedType == nil) {
                    grabbedType = @"";
                }
                if (grabbedCreator == nil) {
                    grabbedCreator = @"";
                }
                [mainWindowTypeTextField setStringValue:grabbedType];
                [mainWindowCreatorTextField setStringValue:grabbedCreator];
            }
        }
    }
    [self resetMainWindowPopUpMenu];
}

- (IBAction)showpresetSheet:(id)sender {
    [self setPresetListSafetyCopy:[self presetList]];
    [self disableFileMenu];
    [presetSheetTableView noteNumberOfRowsChanged];
    [NSApp beginSheet:presetSheet modalForWindow:mainWindow modalDelegate:self didEndSelector:@selector(presetSheetRemoved) contextInfo:nil];
}

- (IBAction)addNewSetting:(id)sender {
    NSString *newType = [self padTypeOrCreatorToFullWidth:[mainWindowTypeTextField stringValue]];
    NSString *newCreator = [self padTypeOrCreatorToFullWidth:[mainWindowCreatorTextField stringValue]];
    [addPresetSheetTypeIndicator setStringValue:newType];
    [addPresetSheetCreatorIndicator setStringValue:newCreator];
    [addPresetSheetNameTextField setStringValue:@"New Setting"];
    [addPresetSheet makeFirstResponder:addPresetSheetNameTextField];
    [self disableFileMenu];
    [NSApp beginSheet:addPresetSheet modalForWindow:mainWindow modalDelegate:self didEndSelector:@selector(addPresetSheetRemoved:) contextInfo:nil];
}

- (IBAction)acceptNewSetting:(id)sender {
    NSString *nameOfNewSetting = [addPresetSheetNameTextField stringValue];
    NSString *typeOfNewSetting = [addPresetSheetTypeIndicator stringValue];
    NSString *creatorOfNewSetting = [addPresetSheetCreatorIndicator stringValue];
    NSMutableArray *presetListCopy = [[self presetList] mutableCopy];
    [presetListCopy addObject:[NSArray arrayWithObjects:nameOfNewSetting, typeOfNewSetting, creatorOfNewSetting, nil]];
    [self setPresetList:presetListCopy];
    [self populateMenus];
    NSUInteger currentPopUpButtonSelection = [[self presetList] count];
    [mainWindowSettingsPopUpButton selectItemAtIndex:currentPopUpButtonSelection];
    [self storePresets];
    [self cancelNewSetting:nil];
    [presetSheetTableView reloadData];
}

- (IBAction)cancelNewSetting:(id)sender {
    [NSApp endSheet:addPresetSheet];
    [addPresetSheet orderOut:self];
}

- (IBAction)mainWindowSettingsPopUpButtonSelectionHasChanged:(id)sender {
    NSUInteger theObject = [[sender representedObject] intValue];
    [self enterTypeAndCreatorFromMenuSelection:theObject];
}

- (IBAction)setFileInfo:(id)sender {
    NSString *theFile = [self fileNameWithFullPath];
    NSString *typeToSet = [mainWindowTypeTextField stringValue];
    NSString *creatorToSet = [mainWindowCreatorTextField stringValue];
    if ([typeToSet isEqualToString:@""]) {
        typeToSet = zeroString;
    } else {
		typeToSet = [self padTypeOrCreatorToFullWidth:typeToSet];
	}
    if ([creatorToSet isEqualToString:@""]) {
        creatorToSet = zeroString;
    } else {
		creatorToSet = [self padTypeOrCreatorToFullWidth:creatorToSet];
	}
    [self setFileTypeAndCreatorForFile:theFile withType:typeToSet withCreator:creatorToSet];
    if ([typeToSet isEqualToString:zeroString]) {
        [mainWindowTypeIndicator setStringValue:@"(none)"];
    } else {
        [mainWindowTypeIndicator setStringValue:typeToSet];
    }
    if ([creatorToSet isEqualToString:zeroString]) {
        [mainWindowCreatorIndicator setStringValue:@"(none)"];
    } else {
        [mainWindowCreatorIndicator setStringValue:creatorToSet];
    }
    NSImage *newImage = [[NSWorkspace sharedWorkspace] iconForFile:theFile];
    [mainWindowImageWell setImage:newImage];
}


#pragma mark Application Delegate Methods

- (BOOL)applicationShouldHandleReopen:(id)sender hasVisibleWindows:(BOOL)flag {
    if (!flag) {
        [mainWindow makeKeyAndOrderFront:self];
    }
    return YES;
}

- (BOOL)application:(NSApplication *)sender openFile:(NSString *)filename {
    NSLog(@"%@", filename);
    return YES;
}

- (BOOL)application:(NSApplication *)sender openFiles:(NSArray *)filenames {
    if (([filenames count] == 1) || (!appIsAlreadyOpen) || (!preferencesBatchDrop)) {
		BOOL isDir;
        NSString *theFileDrop = [filenames objectAtIndex:0];
		BOOL ispackage = [[NSWorkspace sharedWorkspace] isFilePackageAtPath:theFileDrop];
		if (ispackage) {
			[self setFileNameWithFullPath:theFileDrop];
			[self setFileName:[[self fileNameWithFullPath] lastPathComponent]];
			[self loadBundleFileInImageWell];
			return YES;
		}
		BOOL result = [[NSFileManager defaultManager] fileExistsAtPath:theFileDrop isDirectory:&isDir];
		if (!result || isDir) {
			if (appIsAlreadyOpen) {
				[mainWindowLabelInBox setStringValue:@"Cannot process folders."];
				[self prepareForLabelInBoxReset];
			}
			return NO;
		}
		[self setFileNameWithFullPath:theFileDrop];
        [self setFileName:[[self fileNameWithFullPath] lastPathComponent]];
        [self loadFlatFileInImageWell];
    } else {
        [self updateMultipleFiles:filenames];
    }
    return YES;
}

- (void)applicationWillFinishLaunching:(NSNotification *)notification {
    [self getOSVersion];
    [self setFileName:[[NSString alloc] initWithString:@"dummy.file"]];
    [self setFileNameWithFullPath:[[NSString alloc] init]];
    [self setPreviousType:[[NSString alloc] init]];
    [self setPreviousCreator:[[NSString alloc] init]];
    [self setPresetList:[[NSMutableArray alloc] init]];
    [self setPresetListSafetyCopy:[[NSArray alloc] init]];
    [self setPassedURLList:[[NSArray alloc] init]];
    [self setPassedURL:[[NSString alloc] init]];
    zeroString = [[NSString alloc] initWithString:[NSString stringWithFormat:@"%c%c%c%c", 0,0,0,0]];
    [self getPreferences];
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *pathToHelpFile = [appBundle pathForResource:@"Help" ofType:@"rtfd"];
    [mainWindowImageWell registerForDraggedTypes:[NSArray arrayWithObjects:NSFilenamesPboardType, nil]];
    [mainWindowBox registerForDraggedTypes:[NSArray arrayWithObjects:NSFilenamesPboardType, nil]];
    [self presetSheetTableViewSizeCheck];
    [presetSheetTableView registerForDraggedTypes:[NSArray arrayWithObjects:PrivateTableViewDataType, nil]];
    [helpWindowTextView readRTFDFromFile:pathToHelpFile];
    [self populateMenus];
    [mainWindowSettingsPopUpButton setAutoenablesItems:NO];
    [menuFile setAutoenablesItems:NO];
    [self clearCurrentlyLoadedFile:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidEndEditing:) name:NSControlTextDidEndEditingNotification object:presetSheetTableView];
    [self setTimerForAppOpen];
	[mainWindow makeKeyAndOrderFront:self];
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    [self setPreferences];
	if (fileName) {
		[fileName release];
	}
	if (fileNameWithFullPath) {
		[fileNameWithFullPath release];
	}
	if (previousType) {
		[previousType release];
	}
	if (previousCreator) {
		[previousCreator release];
	}
	if (presetList) {
		[presetList release];
	}
	if (presetListSafetyCopy) {
		[presetListSafetyCopy release];
	}
	if (passedURLList) {
		[passedURLList release];
	}
	if (passedURL) {
		[passedURL release];
	}
	if (zeroString) {
		[zeroString release];
	}
    return NSTerminateNow;
}


#pragma mark Application Methods

- (void)appMenuSelectionHasChanged:(id)sender {
    NSUInteger theObject = [[sender representedObject] intValue] + 1;
    [mainWindowSettingsPopUpButton selectItemAtIndex:theObject];
    [self enterTypeAndCreatorFromMenuSelection:theObject - 1];
}

- (void)presetSheetRemoved {
    [self enableFileMenu];
    [presetSheetTableView deselectAll:nil];
}

- (void)preferencesSheetRemoved {
    [self enableFileMenu];
}

- (IBAction)addPresetSheetRemoved:(id)sender {
    [self enableFileMenu];
}

- (void)getPreferences {
    NSUserDefaults *preferencesForUser = [NSUserDefaults standardUserDefaults];
    NSDictionary *defaultPrefs = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSNumber numberWithBool:YES],    @"batchDrop",
                                  [NSNumber numberWithBool:NO],     @"rememberPos",
                                  @"",     @"windowPos",
                                  [NSArray arrayWithObjects:
                                   [NSArray arrayWithObjects: @"Acrobat PDF", @"PDF ", @"CARO", nil],
                                   [NSArray arrayWithObjects: @"Adobe PhotoShop", @"8BPS", @"8BIM", nil],
                                   [NSArray arrayWithObjects: @"Preview JPEG", @"JPEG", @"prvw", nil],
                                   [NSArray arrayWithObjects: @"Preview EPS", @"EPSF", @"prvw", nil],
                                   [NSArray arrayWithObjects: @"TextEdit File", @"TEXT ", @"ttxt", nil],
                                   [NSArray arrayWithObjects: @"Microsoft Word", @"WORD", @"MSWD", nil],
                                   [NSArray arrayWithObjects: @"Microsoft Excel", @"XLS ", @"XCEL", nil],
                                   [NSArray arrayWithObjects: @"iTunes MP3 File", @"mp3 ", @"hook", nil],
                                   [NSArray arrayWithObjects: @"Macintosh Sound Resource", @"snd ",@"RSED", nil],
                                   [NSArray arrayWithObjects: @"QuickTime Movie", @"Moov", @"TVOD", nil],
                                   [NSArray arrayWithObjects: @"QuickTime MIDI File", @"Midi", @"TVOD", nil],
                                   [NSArray arrayWithObjects: @"StuffIt! Archive", @"SIT!", @"SITx", nil],
                                   nil], @"presets", nil];
    [preferencesForUser registerDefaults:defaultPrefs];
    preferencesBatchDrop = [preferencesForUser boolForKey:@"batchDrop"];
    [preferencesSheetBatchDropCheckBox setState:preferencesBatchDrop];
    NSString *positionString = [preferencesForUser objectForKey:@"windowPos"];
    [preferencesSheetRememberPositionCheckBox setState:preferencesRememberPosition];
    preferencesRememberPosition = [preferencesForUser boolForKey:@"rememberPos"];
    [preferencesSheetRememberPositionCheckBox setState:preferencesRememberPosition];
    if (preferencesRememberPosition) {
        if ([positionString isNotEqualTo: @""]) {
            preferencesPosition = NSPointFromString(positionString);
        } else {
            preferencesPosition = [self getDefaultWindowPosition];
        }
    } else {
        preferencesPosition = [self getDefaultWindowPosition];
    }
    [mainWindow setFrameOrigin:preferencesPosition];
    [self setPresetList:[preferencesForUser objectForKey:@"presets"]];
}

- (void)setPreferences {
    NSUserDefaults *preferencesForUser = [NSUserDefaults standardUserDefaults];
    preferencesBatchDrop = [preferencesSheetBatchDropCheckBox state];
    preferencesRememberPosition = [preferencesSheetRememberPositionCheckBox state];
    [preferencesForUser setBool:preferencesBatchDrop forKey:@"batchDrop"];
    [preferencesForUser setBool:preferencesRememberPosition forKey:@"rememberPos"];
    preferencesPosition = [mainWindow frame].origin;
    NSString *positionString = NSStringFromPoint(preferencesPosition);
    [preferencesForUser setObject:positionString forKey:@"windowPos"];
}

- (void)setTimerForImageWellDragItem {
    allPurposeTimer = [NSTimer scheduledTimerWithTimeInterval:0.10 target:self selector:@selector(receivedDragItemInImageWell) userInfo:nil repeats:NO];
}

- (void)setTimerForBoxDragItems {
    allPurposeTimer = [NSTimer scheduledTimerWithTimeInterval:0.10 target:self selector:@selector(receivedDragItemsInBox) userInfo:nil repeats:NO];
}

- (void)receivedDragItemInImageWell {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSWorkspace *workSpace = [NSWorkspace sharedWorkspace];
    [NSApp activateIgnoringOtherApps:YES];
    [self setFileNameWithFullPath:[self passedURL]];
    [self setFileName:[[self fileNameWithFullPath] lastPathComponent]];
    BOOL fileExists = [fileManager fileExistsAtPath:[self fileNameWithFullPath]];
    BOOL isBundle = [workSpace isFilePackageAtPath:[self fileNameWithFullPath]];
    if (!fileExists) {
        [mainWindowLabelInBox setStringValue:@"File not accessible."];
        [self prepareForLabelInBoxReset];
        return;
    }
    if (!isBundle) {
        [self loadFlatFileInImageWell];
    } else {
        [self loadBundleFileInImageWell];
    }
    [self resetMainWindowPopUpMenu];
}

- (void)fileWasPickedWithOpenButton {
    NSWorkspace *workSpace = [NSWorkspace sharedWorkspace];
    [self setFileName:[fileNameWithFullPath lastPathComponent]];
    BOOL result = [workSpace isFilePackageAtPath:[self fileNameWithFullPath]];
    if (!result) {
        [self loadFlatFileInImageWell];
    } else {
        [self loadBundleFileInImageWell];
    }
    [self resetMainWindowPopUpMenu];
}

- (void)receivedDragItemsInBox {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSWorkspace *workSpace = [NSWorkspace sharedWorkspace];
    NSArray *theFiles = [self passedURLList];
    int filesSkipped = 0;
    NSUInteger numberOfItems = [theFiles count];
    NSString *typeToSet = [mainWindowTypeTextField stringValue];
    NSString *creatorToSet = [mainWindowCreatorTextField stringValue];
    NSString *currentItem;
    NSString *newLabelInBoxText;
    BOOL fileExists;
    BOOL fileIsPackage;
    BOOL isDir;
    int i;
    [NSApp activateIgnoringOtherApps:YES];
    if ([typeToSet isEqualToString:@""]) {
        typeToSet = zeroString;
    } else {
		typeToSet = [self padTypeOrCreatorToFullWidth:typeToSet];
	}
    if ([creatorToSet isEqualToString:@""]) {
        creatorToSet = zeroString;
    } else {
		creatorToSet = [self padTypeOrCreatorToFullWidth:creatorToSet];
	}
    for (i = 0; i < numberOfItems; i++) {
        currentItem = [theFiles objectAtIndex:i];
        fileExists = [fileManager fileExistsAtPath:currentItem isDirectory:&isDir];
        fileIsPackage = [workSpace isFilePackageAtPath:currentItem];
        if ( isDir || !fileExists || fileIsPackage ) {
            filesSkipped++;
        } else {
            [self setFileTypeAndCreatorForFile:currentItem withType:typeToSet withCreator:creatorToSet];
        }
    }
    numberOfItems -= filesSkipped;
    if (numberOfItems == 1) {
        newLabelInBoxText = [NSString stringWithFormat:@"%lu item processed.", (unsigned long)numberOfItems];
    } else if (numberOfItems == 0) {
        newLabelInBoxText = @"No items processed.";
    } else {
        newLabelInBoxText = [NSString stringWithFormat:@"%lu items processed.", (unsigned long)numberOfItems];
    }
    [mainWindowLabelInBox setStringValue:newLabelInBoxText];
    [self prepareForLabelInBoxReset];
}

- (void)setFileTypeAndCreatorForFile:(NSString *)theFile withType: (NSString *)theType withCreator:(NSString *)theCreator {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    OSType theFileType = NSHFSTypeCodeFromFileType([NSString stringWithFormat:@"'%@'", theType]);
    OSType theFileCreator = NSHFSTypeCodeFromFileType([NSString stringWithFormat:@"'%@'", theCreator]);
    NSNumber* typeCode = [NSNumber numberWithUnsignedLong:theFileType];
    NSNumber* creatorCode = [NSNumber numberWithUnsignedLong:theFileCreator];
    NSDictionary *ftc = [NSDictionary dictionaryWithObjectsAndKeys: typeCode, NSFileHFSTypeCode,
                         creatorCode, NSFileHFSCreatorCode, nil];
    [fileManager changeFileAttributes:ftc atPath:(NSString *)theFile];
}

- (void)loadFlatFileInImageWell {
    NSImage *newImage = [[NSWorkspace sharedWorkspace] iconForFile:[self fileNameWithFullPath]];
    [mainWindowImageWell setImage:newImage];
    [self enableButtonsAndSetFirstResponder];
    NSString *theFile = [self fileNameWithFullPath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSDictionary *attribDict = [fileManager fileAttributesAtPath:theFile traverseLink:NO];
    OSType currentOSType = [[attribDict objectForKey:@"NSFileHFSTypeCode"] unsignedIntValue];
    OSType currentOSCreator = [[attribDict objectForKey:@"NSFileHFSCreatorCode"] unsignedIntValue];
    NSString *currentQuotedType = NSFileTypeForHFSTypeCode(currentOSType);
    NSString *currentQuotedCreator = NSFileTypeForHFSTypeCode(currentOSCreator);
    NSString *currentType = [self trimmedString:currentQuotedType];
    NSString *currentCreator = [self trimmedString:currentQuotedCreator];
    [self setPreviousType:currentType];
    [self setPreviousCreator:currentCreator];
    [mainWindowFileLabel setStringValue:[self fileName]];
    [mainWindowFileLabel setToolTip:[NSString stringWithFormat:@"Currently selected file: %@", [self fileName]]];
    [mainWindowImageWell setToolTip:[NSString stringWithFormat:@"File: %@", [self fileName]]];
    if ([currentType isEqualToString:zeroString]) {
        [mainWindowTypeIndicator setStringValue:@"(none)"];
        [mainWindowTypeTextField setStringValue:@""];
    } else {
        [mainWindowTypeIndicator setStringValue:[self previousType]];
        [mainWindowTypeTextField setStringValue:[self previousType]];
    }
    if ([currentType isEqualToString:zeroString]) {
        [mainWindowCreatorIndicator setStringValue:@"(none)"];
        [mainWindowCreatorTextField setStringValue:@""];
    } else {
        [mainWindowCreatorIndicator setStringValue:[self previousCreator]];
        [mainWindowCreatorTextField setStringValue:[self previousCreator]];
    }
    [mainWindowSetButton setEnabled:YES];
    [mainWindowSetButton setToolTip:@"Sets the selected file above to the file type and creator on the left."];
}

- (void)loadBundleFileInImageWell {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir;
    NSString *plistInfo = [NSString stringWithFormat:@"%@/Contents/Info.plist", [self fileNameWithFullPath]];
    BOOL fileExists = [fileManager fileExistsAtPath:plistInfo isDirectory:&isDir];
    if (!fileExists) {
        [mainWindowTypeTextField setStringValue:@""];
        [mainWindowCreatorTextField setStringValue:@""];
        [mainWindowTypeIndicator setStringValue:@"(none)"];
        [mainWindowCreatorIndicator setStringValue:@"(none)"];
        [mainWindowLabelInBox setStringValue:@"No bundle info - loaded read only."];
    } else {
        NSDictionary *infoPlist = [NSDictionary dictionaryWithContentsOfFile:plistInfo];
        NSString *fetchedType = [infoPlist objectForKey:@"CFBundlePackageType"];
        NSString *fetchedCreator = [infoPlist objectForKey:@"CFBundleSignature"];
        NSString *fetchedTypeIndicator = fetchedType;
        NSString *fetchedCreatorIndicator = fetchedCreator;
        if (fetchedType == nil) {
            fetchedType = @"";
            fetchedTypeIndicator = @"(none)";
        }
        if (fetchedCreator == nil) {
            fetchedCreator = @"";
            fetchedCreatorIndicator = @"(none)";
        }
        [mainWindowTypeTextField setStringValue:fetchedType];
        [mainWindowCreatorTextField setStringValue:fetchedCreator];
        [mainWindowTypeIndicator setStringValue:fetchedTypeIndicator];
        [mainWindowCreatorIndicator setStringValue:fetchedCreatorIndicator];
        [mainWindowLabelInBox setStringValue:@"Bundles load read only."];
    }
    NSImage *newImage = [[NSWorkspace sharedWorkspace] iconForFile:[self fileNameWithFullPath]];
    [mainWindowImageWell setImage:newImage];
    [self enableButtonsAndSetFirstResponder];
    [mainWindowFileLabel setStringValue:[self fileName]];
    [mainWindowSetButton setEnabled:NO];
    [mainWindowSetButton setToolTip:@"Sets the selected file above to the file type and creator on the left. Currently not available because the type and creator code of bundles cannot be set in this manner."];
    [self prepareForLabelInBoxReset];
}

- (void)updateMultipleFiles:(NSArray *)thePaths {
    [self setPassedURLList:thePaths];
    [self receivedDragItemsInBox];
}

- (void)enableButtonsAndSetFirstResponder {
    if ([mainWindowSetButton isEnabled] == NO) {
        [menuItemSet setEnabled:YES];
        [mainWindowSetButton setEnabled:YES];
        [mainWindowSetButton setToolTip:@"Sets the selected file above to the file type and creator on the left."];
        [menuItemReset setEnabled:YES];
        [mainWindowResetButton setEnabled:YES];
        [mainWindowResetButton setToolTip:@"Reverts to the file type and creator values as they were before editing."];
        [menuItemClear setEnabled:YES];
        [mainWindowClearButton setEnabled:YES];
        [mainWindowClearButton setToolTip:@"Click here to clear the current selection."];
    }
    [self setFirstResponderAfterGrab];
}

- (void)setFirstResponderAfterGrab {
    [mainWindow makeFirstResponder:mainWindowTypeTextField];
}

- (NSString *)padTypeOrCreatorToFullWidth:(NSString *)theString {
    if ([theString length] == 4) {
        return theString;
    }
    NSString *padString = @"    ";
    NSString *extraSpaces = [padString substringWithRange:NSMakeRange(0, (4 - [theString length]))];
    return [NSString stringWithFormat:@"%@%@", theString, extraSpaces];
}

- (void)prepareForLabelInBoxReset {
    allPurposeTimer = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(doActualLabelInBoxReset) userInfo:nil repeats:NO];
}

- (void)doActualLabelInBoxReset {
    [mainWindowLabelInBox setStringValue:@"Drag and drop files here..."];
}

- (void)setTimerForAppOpen {
    allPurposeTimer = [NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(appIsOpen) userInfo:nil repeats:NO];
}

- (void)appIsOpen {
    appIsAlreadyOpen = YES;
}

- (void)removepresetSheetOnOK {
    [mainWindowSettingsPopUpButton selectItemAtIndex:0];
    [self populateMenus];
    [self storePresets];
    [self setPresetListSafetyCopy:nil];
    [presetSheetTableView deselectAll:nil];
    [NSApp endSheet:presetSheet];
    [presetSheet orderOut:self];
}

- (void)removepresetSheetOnCancel {
    [self setPresetList:[[self presetListSafetyCopy] mutableCopy]];
    [mainWindowSettingsPopUpButton selectItemAtIndex:0];
    [self populateMenus];
    [self storePresets];
    [NSApp endSheet:presetSheet];
    [presetSheet orderOut:self];
    [presetSheetTableView reloadData];
}

- (void)populateMenus {
    int i;
    [menuItemSettings setEnabled:NO];
    [mainWindowSettingsPopUpButton setEnabled:NO];
    NSUInteger itemCount = [menuSubSettings numberOfItems];
    if (itemCount > 0) {
        for (i = (int)itemCount; i > 0; i--) {
            [menuSubSettings removeItemAtIndex:i - 1];
        }
    }
    [mainWindowSettingsPopUpButton removeAllItems];
    NSMenuItem *firstPopUpMenuItem = [[[NSMenuItem alloc] initWithTitle:@"Please select a preset" action:nil keyEquivalent:@""] autorelease];
    [firstPopUpMenuItem setEnabled:NO];
    [[mainWindowSettingsPopUpButton menu] addItem:firstPopUpMenuItem];
    if ([[self presetList] count] > 0) {
        NSMenuItem *newAppMenuItem;
        NSMenuItem *newPopUpButtonMenuItem;
        for (i = 0; i < [[self presetList] count]; i++) {
            NSArray *currentListPos = [[self presetList] objectAtIndex:i];
            NSString *currentItem = [currentListPos objectAtIndex:0];
            newAppMenuItem = [[[NSMenuItem alloc] initWithTitle:currentItem action:@selector(appMenuSelectionHasChanged:) keyEquivalent:@""] autorelease];
            newPopUpButtonMenuItem = [[[NSMenuItem alloc] initWithTitle:currentItem action:@selector(mainWindowSettingsPopUpButtonSelectionHasChanged:) keyEquivalent:@""] autorelease];
            [newAppMenuItem setEnabled:YES];
            [newAppMenuItem setTarget:self];
            [newAppMenuItem setRepresentedObject:[NSNumber numberWithInt:i]];
            [newPopUpButtonMenuItem setEnabled:YES];
            [newPopUpButtonMenuItem setTarget:self];
            [newPopUpButtonMenuItem setRepresentedObject:[NSNumber numberWithInt:i]];
            [menuSubSettings addItem:newAppMenuItem];
            [[mainWindowSettingsPopUpButton menu] addItem:newPopUpButtonMenuItem];
        }
    }
    NSMenuItem *newAppMenuItem = [NSMenuItem separatorItem];
    NSMenuItem *newPopUpButtonMenuItem = [NSMenuItem separatorItem];
    [menuSubSettings addItem:newAppMenuItem];
    [[mainWindowSettingsPopUpButton menu] addItem:newPopUpButtonMenuItem];
    newAppMenuItem = [[[NSMenuItem alloc] initWithTitle:@"Edit this menu..." action:@selector(showpresetSheet:) keyEquivalent:@"e"] autorelease];
    newPopUpButtonMenuItem = [[[NSMenuItem alloc] initWithTitle:@"Edit this menu..." action:@selector(showpresetSheet:) keyEquivalent:@""] autorelease];
    [newAppMenuItem setEnabled:YES];
    [newAppMenuItem setTarget:self];
    [newPopUpButtonMenuItem setEnabled:YES];
    [newPopUpButtonMenuItem setTarget:self];
    [menuSubSettings addItem:newAppMenuItem];
    [[mainWindowSettingsPopUpButton menu] addItem:newPopUpButtonMenuItem];
    [menuItemSettings setEnabled:YES];
    [mainWindowSettingsPopUpButton setEnabled:YES];
}

- (void)disableFileMenu {
    [menuFileTop setEnabled:NO];
}

- (void)enableFileMenu {
    [menuFileTop setEnabled:YES];
}

- (void)enterTypeAndCreatorFromMenuSelection:(NSUInteger)theNumber {
    NSArray *theSelection = [[self presetList] objectAtIndex:theNumber];
    NSString *newType = [theSelection objectAtIndex:1];
    NSString *newCreator = [theSelection objectAtIndex:2];
    [mainWindowTypeTextField setStringValue:newType];
    [mainWindowCreatorTextField setStringValue:newCreator];
}

- (void)storePresets {
    [[NSUserDefaults standardUserDefaults] setObject:[self presetList] forKey:@"presets"];
}

- (NSPoint)getDefaultWindowPosition {
    NSScreen *screen = [NSScreen mainScreen];
    NSRect screenRect = [screen visibleFrame];
    float x = (screenRect.size.width / 2) - 156;
    float y = (screenRect.size.height / 2) - 174;
    return NSMakePoint(x, y);
}

- (void)resetMainWindowPopUpMenu {
    [mainWindowSettingsPopUpButton selectItemAtIndex:0];
}

- (NSString *)trimmedString:(NSString *)string {
    return [string substringWithRange:NSMakeRange(1, [string length] - 2)];
}

- (NSString *)deleteCharFromString:(NSString *)string atLocation:(NSUInteger)location {
    NSString *lowerPart = [string substringWithRange:NSMakeRange(0, location)];
    NSString *upperPart = [string substringWithRange:NSMakeRange(location + 1, [string length] - (location + 1))];
    return [NSString stringWithFormat:@"%@%@", lowerPart, upperPart];
}

- (void)textDidEndEditing:(id)sender {
    NSDictionary *userinfo = [sender userInfo];
    NSUInteger textmovement = [[userinfo objectForKey:@"NSTextMovement"] intValue];
    if (textmovement == NSReturnTextMovement) {
        rowToPass = [presetSheetTableView selectedRow];
		NSRect visibleRect =[[presetSheetScrollView contentView] documentVisibleRect];
		originalScrollPos = visibleRect.origin;
        [self performSelector:@selector(resetScrollViewPosition:) withObject:self afterDelay:0.0];
		[self performSelector:@selector(resetRowSelection:) withObject:self afterDelay:0.0];
        [presetSheet performSelector:@selector(makeFirstResponder:) withObject:presetSheetTableView afterDelay:0.0];
    }
}

- (void)resetRowSelection:(id)sender {
    NSIndexSet *indexset = [NSIndexSet indexSetWithIndex:rowToPass];
    [presetSheetTableView selectRowIndexes:indexset byExtendingSelection:NO];
}

- (void)resetScrollViewPosition:(id)sender {
	[[presetSheetScrollView contentView] scrollToPoint:originalScrollPos];
	[presetSheetScrollView reflectScrolledClipView:[presetSheetScrollView contentView]];
}

- (void)getOSVersion {
    NSString *theVersionString = [[NSProcessInfo processInfo] operatingSystemVersionString];
    NSArray *versionTextItems = [theVersionString componentsSeparatedByString:@"."];
    OSVersion = [[versionTextItems objectAtIndex:1] intValue];
}

- (void)presetSheetTableViewSizeCheck {
    if (OSVersion >= 10) {
        [presetSheetScrollView setFrame:NSMakeRect(20, 54, 340, 252)];
    }
    if (OSVersion >= 7) {
        [[presetSheetTableView tableColumnWithIdentifier:@"name"] setWidth:229];
        [presetSheetScrollView setHorizontalScrollElasticity:1];
    }
}

@end
