//
//  AppDelegate.h
//  File Type & Creator Editor
//
//  Created by Petros on 8/30/17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>


#pragma mark Constants
NSString *const PrivateTableViewDataType;


#pragma mark Interface
@interface AppDelegate : NSObject {

    
#pragma mark IBOutlets
    

#pragma mark IBOutlets : Main Window
    IBOutlet NSBox *mainWindowBox;
    IBOutlet NSWindow *mainWindow;
    IBOutlet NSButton *mainWindowSetButton;
    IBOutlet NSButton *mainWindowResetButton;
    IBOutlet NSButton *mainWindowClearButton;
    IBOutlet NSButton *mainWindowGrabButton;
    IBOutlet NSImageView *mainWindowImageWell;
    IBOutlet NSTextField *mainWindowLabelInBox;
    IBOutlet NSTextField *mainWindowTypeIndicator;
    IBOutlet NSTextField *mainWindowCreatorIndicator;
    IBOutlet NSTextField *mainWindowTypeTextField;
    IBOutlet NSTextField *mainWindowCreatorTextField;
    IBOutlet NSTextField *mainWindowFileLabel;
    IBOutlet NSPopUpButton *mainWindowSettingsPopUpButton;

    
#pragma mark IBOutlets : Preset Sheet
    IBOutlet NSWindow *presetSheet;
    IBOutlet NSTableView *presetSheetTableView;
    IBOutlet NSScrollView *presetSheetScrollView;
    
    
#pragma mark IBOutlets : Add Preset Sheet
    IBOutlet NSWindow *addPresetSheet;
    IBOutlet NSTextField *addPresetSheetNameTextField;
    IBOutlet NSTextField *addPresetSheetTypeIndicator;
    IBOutlet NSTextField *addPresetSheetCreatorIndicator;
    
    
#pragma mark IBOutlets : Preferences Sheet
    IBOutlet NSWindow *preferencesSheet;
    IBOutlet NSButton *preferencesSheetBatchDropCheckBox;
    IBOutlet NSButton *preferencesSheetRememberPositionCheckBox;

    
#pragma mark IBOutlets : Help Window
    IBOutlet NSWindow *helpWindow;
    IBOutlet NSTextView *helpWindowTextView;
	
    
#pragma mark IBOutlets : Menu-related
    IBOutlet NSMenu *menuFile;
    IBOutlet NSMenu *menuSubSettings;
    IBOutlet NSMenuItem *menuFileTop;
    IBOutlet NSMenuItem *menuItemClear;
    IBOutlet NSMenuItem *menuItemSet;
    IBOutlet NSMenuItem *menuItemReset;
    IBOutlet NSMenuItem *menuItemSettings;

    
#pragma mark Application Variables
    BOOL firstOpen;
    BOOL fileMenuDisabled;
    BOOL appIsAlreadyOpen;
    NSArray *presetList;
    NSArray *presetListSafetyCopy;
    NSTimer *allPurposeTimer;
    NSString *zeroString;
    NSString *fileName;
    NSString *fileNameWithFullPath;
    NSString *previousType;
    NSString *previousCreator;
    int OSVersion;
    
    
#pragma mark Inter-class Variables
    NSArray *passedURLList;
    NSString *passedURL;
	NSPoint originalScrollPos;
	int rowToPass;

    
#pragma mark Preferences Related Variables
    BOOL preferencesBatchDrop;
    BOOL preferencesRememberPosition;
    NSPoint preferencesPosition;

}


#pragma mark Getters and Setters

- (NSString *)zeroString;
- (NSArray *)presetList;
- (void)setPresetList:(NSMutableArray *)array;
- (NSString *)passedURL;
- (void)setPassedURL:(NSString *)passedPath;
- (NSArray *)passedURLList;
- (void)setPassedURLList:(NSArray *)passedList;
- (NSArray *)presetListSafetyCopy;
- (void)setPresetListSafetyCopy:(NSArray *)array;
- (NSString *)fileName;
- (void)setFileName:(NSString *)name;
- (NSString *)fileNameWithFullPath;
- (void)setFileNameWithFullPath:(NSString *)path;
- (NSString *)previousType;
- (void)setPreviousType:(NSString *)prevtype;
- (NSString *)previousCreator;
- (void)setPreviousCreator:(NSString *)prevcreator;
- (NSUInteger)OSVersion;


#pragma mark IBActions

- (IBAction)showpreferencesSheet:(id)sender;
- (IBAction)updatePreferences:(id)sender;
- (IBAction)cancelPreferences:(id)sender;
- (IBAction)showHelpWindow:(id)sender;
- (IBAction)getFileToLoad:(id)sender;
- (IBAction)clearCurrentlyLoadedFile:(id)sender;
- (IBAction)restorePreviousTypeAndCreator:(id)sender;
- (IBAction)grabFileInfo:(id)sender;
- (IBAction)showpresetSheet:(id)sender;
- (IBAction)addNewSetting:(id)sender;
- (IBAction)acceptNewSetting:(id)sender;
- (IBAction)cancelNewSetting:(id)sender;
- (IBAction)mainWindowSettingsPopUpButtonSelectionHasChanged:(id)sender;
- (IBAction)setFileInfo:(id)sender;


#pragma mark Delegate Methods

- (BOOL)applicationShouldHandleReopen:(id)sender hasVisibleWindows:(BOOL)flag;
- (BOOL)application:(NSApplication *)sender openFile:(NSString *)filename;
- (BOOL)application:(NSApplication *)sender openFiles:(NSArray *)filenames;
- (void)applicationWillFinishLaunching:(NSNotification *)notification;
- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender;


#pragma mark Application Methods

- (void)appMenuSelectionHasChanged:(id)sender;
- (void)addPresetSheetRemoved:(id)sender;
- (void)presetSheetRemoved;
- (void)preferencesSheetRemoved;
- (void)getPreferences;
- (void)setPreferences;
- (void)setTimerForImageWellDragItem;
- (void)setTimerForBoxDragItems;
- (void)receivedDragItemInImageWell;
- (void)fileWasPickedWithOpenButton;
- (void)receivedDragItemsInBox;
- (void)setFileTypeAndCreatorForFile:(NSString *)theFile withType: (NSString *)theType withCreator:(NSString *)theCreator;
- (void)loadFlatFileInImageWell;
- (void)loadBundleFileInImageWell;
- (void)updateMultipleFiles:(NSArray *)thePaths;
- (void)enableButtonsAndSetFirstResponder;
- (void)setFirstResponderAfterGrab;
- (NSString *)padTypeOrCreatorToFullWidth:(NSString *)theString;
- (void)prepareForLabelInBoxReset;
- (void)doActualLabelInBoxReset;
- (void)setTimerForAppOpen;
- (void)appIsOpen;
- (void)removepresetSheetOnOK;
- (void)removepresetSheetOnCancel;
- (void)populateMenus;
- (void)disableFileMenu;
- (void)enableFileMenu;
- (void)enterTypeAndCreatorFromMenuSelection:(NSUInteger)theNumber;
- (void)storePresets;
- (NSPoint)getDefaultWindowPosition;
- (void)resetMainWindowPopUpMenu;
- (NSString *)trimmedString:(NSString *)string;
- (NSString *)deleteCharFromString:(NSString *)string atLocation:(NSUInteger)location;
- (void)textDidEndEditing:(id)sender;
- (void)resetRowSelection:(id)sender;
- (void)resetScrollViewPosition:(id)sender;
- (void)getOSVersion;
- (void)presetSheetTableViewSizeCheck;

@end


