# File Type & Creator Editor #
A utility for changing the file type & creator codes of files on MacOS file systems in situations where it still may be relevant. Source made available under an MIT license. Ready to use binaries for Intel and PowerPC available from downloads section. Tested on a variety of OS versions, please see the help file (Help.rtfd). The PowerPC version should run on anything from 10.1.5 thru 10.5.8 (with graceful degradation of unavailable features on lower OS versions), while the Intel version should run on anything from 10.4 thru the current version os macOS (10.13 as of writing this).

Building
--------
PowerPC project is Xcode 1.5; the binary has been built on 10.3.9. Intel project is Xcode 3.1; binary was built on 10.6.8 Server in a VM. Although the PowerPC code builds and runs without a hitch on modern Intel systems (if you upgrade the XCode project), the Intel version takes advantage of features not available in earlier OS versions.

Current version is 1.0 (build 18) for PowerPC, 1.0 (build 21) for Intel. Bar any big problems, the PowerPC version will probably not be updated anymore. Intel version will be updated when deprecated methods are actually removed in the future.
