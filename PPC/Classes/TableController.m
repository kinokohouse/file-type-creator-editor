//
//  TableController.m
//  File Type & Creator Editor
//
//  Created by Petros on 8/30/17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//


#import "TableController.h"


@implementation TableController


#pragma mark Getters and Setters

- (NSArray *) selectedRows {
    NSEnumerator *theEnum = [presetSheetTableView selectedRowEnumerator];
    NSNumber *rowNumber;
    NSMutableArray *rowNumberArray = [NSMutableArray arrayWithCapacity:[presetSheetTableView numberOfSelectedRows]];
    while (nil != (rowNumber = [theEnum nextObject]) )
    {
        [rowNumberArray addObject:rowNumber];
    }
    return rowNumberArray;
}


#pragma mark IBActions

- (IBAction)addNewTableRow:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    [self forceEndEditing];
    NSMutableArray *presetList = [[appDelegate presetList] mutableCopy];
    NSUInteger countOfRows = [presetList count];
    [presetList addObject:[NSMutableArray arrayWithObjects:@"New Setting", @"????", @"????", nil]];
    [appDelegate setPresetList:presetList];
    if (countOfRows > 11) {
        [[presetSheetScrollView contentView] scrollToPoint:NSMakePoint(0, 17 * countOfRows)];
    }
    [presetSheetTableView reloadData];
    NSUInteger rowToSelect = [presetList count] - 1;
    [presetSheetTableView selectRow:rowToSelect byExtendingSelection:NO];
}

- (IBAction)deleteTableRows:(id)sender {
    int i;
    NSArray *currentSelection = [self selectedRows];
    NSUInteger numberOfRowsToDelete = [currentSelection count];
    if (numberOfRowsToDelete == 0) {
        return;
    }
    [self forceEndEditing];
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    NSMutableArray *presetList = [[appDelegate presetList] mutableCopy];
    for (i = 0; i < [currentSelection count]; i++) {
        [presetList replaceObjectAtIndex:[[currentSelection objectAtIndex:i] intValue] withObject:[NSNull null]];
    }
    [presetList removeObjectIdenticalTo:[NSNull null]];
    [appDelegate setPresetList:presetList];
    [presetSheetTableView deselectAll:nil];
    [presetSheetTableView reloadData];
    [presetSheetTableView noteNumberOfRowsChanged];
}

- (IBAction)wrapUpTableEditing:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    [self forceEndEditing];
    [appDelegate removepresetSheetOnOK];
}

- (IBAction)cancelTableEditing:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    [self forceEndEditing];
    [appDelegate removepresetSheetOnCancel];
}


#pragma mark Table Controller/DataSource Methods

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
	return [[appDelegate presetList] count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    NSArray *presetList = [appDelegate presetList];
    NSArray *currentlyStrobed;
    NSString *currentRowAndCol = @"";
    currentlyStrobed = [presetList objectAtIndex:row];
    if ([[tableColumn identifier] isEqualToString:@"name"]) {
        currentlyStrobed = [presetList objectAtIndex:row];
        currentRowAndCol = [currentlyStrobed objectAtIndex:0];
    }
    if ([[tableColumn identifier] isEqualToString:@"type"]) {
        currentlyStrobed = [presetList objectAtIndex:row];
        currentRowAndCol = [currentlyStrobed objectAtIndex:1];
    }
    if ([[tableColumn identifier] isEqualToString:@"creator"]) {
        currentlyStrobed = [presetList objectAtIndex:row];
        currentRowAndCol = [currentlyStrobed objectAtIndex:2];
    }
    return currentRowAndCol;
}

- (BOOL)tableView:(NSTableView *)tableView writeRows:(NSArray *)rows toPasteboard:(NSPasteboard *)pboard {
    NSData *rowData = [NSArchiver archivedDataWithRootObject:rows];
    [pboard declareTypes:[NSArray arrayWithObjects:PrivateTableViewDataType, nil] owner:self];
    [pboard setData:rowData forType:PrivateTableViewDataType];
    return YES;
}

- (NSDragOperation)tableView:(NSTableView *)tableView validateDrop:(id<NSDraggingInfo>)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)dropOperation {
    [tableView setDropRow:row dropOperation:NSTableViewDropAbove];
    return NSDragOperationMove;
}

- (BOOL)tableView:(NSTableView *)tableView acceptDrop:(id<NSDraggingInfo>)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)dropOperation {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    int i;
    NSUInteger dropCount = 0;
    NSMutableArray *presetList = [[appDelegate presetList] mutableCopy];
    NSPasteboard *thePasteBoard = [info draggingPasteboard];
    NSData *rowData = [thePasteBoard dataForType:PrivateTableViewDataType];
    NSArray *rows = [NSUnarchiver unarchiveObjectWithData:rowData];
    NSMutableArray *keepArray = [NSMutableArray arrayWithCapacity:[rows count]];
    for (i = 0; i < [rows count]; i++) {
        [keepArray addObject:[presetList objectAtIndex:[[rows objectAtIndex:i] intValue]]];
        [presetList replaceObjectAtIndex:[[rows objectAtIndex:i] intValue] withObject:[NSNull null]];
        if ([[rows objectAtIndex:i] intValue] < row) {
            dropCount++;
        }
    }
    [presetList replaceObjectsInRange:NSMakeRange(row, 0) withObjectsFromArray:keepArray];
    [presetList removeObjectIdenticalTo:[NSNull null]];
    [appDelegate setPresetList:presetList];
    [presetSheetTableView reloadData];
    NSInteger selRow = row;
    NSInteger rowCount = [rows count];
    BOOL extendOrNot = NO;
    for (i = 0; i < rowCount; i++) {
        [presetSheetTableView selectRow:(i + selRow - dropCount) byExtendingSelection:extendOrNot];
        extendOrNot = YES;
    }
    return YES;
}


#pragma mark Editing Control Methods

- (void)controlTextDidBeginEditing:(NSNotification *)notification {
    NSDictionary *userdict = [notification userInfo];
    NSText *fieldEditor = [userdict objectForKey:@"NSFieldEditor"];
    originalValue = [fieldEditor string];

}

- (void)controlTextDidChange:(NSNotification *)notification {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    NSObject *object = [notification object];
    NSDictionary *userdict = [notification userInfo];
    NSText *fieldEditor = [userdict objectForKey:@"NSFieldEditor"];
	NSRange fieldEditorSelectedRange = [fieldEditor selectedRange];
    NSUInteger insertPosition = fieldEditorSelectedRange.location;
    NSString *currentText = [[[NSString alloc] initWithString:[fieldEditor string]] autorelease];
    NSInteger currentColumn = [object editedColumn];
    if (currentColumn == 0) {
        return;
    }
    if ([currentText length] > 4) {
        NSBeep();
        currentText = [appDelegate deleteCharFromString:currentText atLocation:insertPosition - 1];
        [fieldEditor setString:currentText];
        [fieldEditor setSelectedRange:NSMakeRange(insertPosition - 1, 0)];
    }
}

- (BOOL)control:(NSControl *)control textShouldEndEditing:(NSText *)fieldEditor {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    NSMutableArray *presetList = [[appDelegate presetList] mutableCopy];
    NSUInteger currentColumn = [control editedColumn];
    NSUInteger currentRow = [control editedRow];
    NSString *currentText = [[[NSString alloc] initWithString:[fieldEditor string]] autorelease];
    if (currentColumn > 0 && [currentText length] < 4) {
        currentText = [appDelegate padTypeOrCreatorToFullWidth:currentText];
    }
    NSMutableArray *rowRepresentation = [[presetList objectAtIndex:currentRow] mutableCopy];
    [rowRepresentation replaceObjectAtIndex:currentColumn withObject:(NSString *)currentText];
    [presetList replaceObjectAtIndex:currentRow withObject:rowRepresentation];
    [appDelegate setPresetList:presetList];
    return YES;
}

- (void)forceEndEditing {
    if ([presetSheetTableView currentEditor]) {
		if (originalValue) {
			[[presetSheetTableView currentEditor] setString:originalValue];
		}
    }
	[presetSheet performSelector:@selector(makeFirstResponder:) withObject:presetSheetTableView afterDelay:0.0];
}

@end
