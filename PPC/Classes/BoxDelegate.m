//
//  BoxDelegate.m
//  File Type & Creator Editor
//
//  Created by Petros on 8/30/17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//

#import "BoxDelegate.h"
#import "AppDelegate.h"

@implementation BoxDelegate

- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender {
    NSPasteboard *pb = [sender draggingPasteboard];
    NSArray *theFiles = [pb propertyListForType:NSFilenamesPboardType];
    if (theFiles == nil) {
        return NO;
    }
    return YES;
}

- (NSDragOperation)performDragOperation:(id<NSDraggingInfo>)sender {
    NSPasteboard *pb = [sender draggingPasteboard];
    NSArray *theFiles = [pb propertyListForType:NSFilenamesPboardType];
    urlList = [[theFiles mutableCopy] retain];
    return YES;
}

- (NSDragOperation)concludeDragOperation:(id<NSDraggingInfo>)sender {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    [appDelegate setPassedURLList:urlList];
    [urlList release];
    [appDelegate setTimerForBoxDragItems];
    return YES;
}

@end
