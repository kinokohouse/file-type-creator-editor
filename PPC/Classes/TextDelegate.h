//
//  TextDelegate.h
//  File Type & Creator Editor
//
//  Created by Petros on 8/30/17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "AppDelegate.h"

@interface TextDelegate : NSTextField

- (void)textDidChange:(NSNotification *)notification;

@end
