//
//  WellDelegate.h
//  File Type & Creator Editor
//
//  Created by Petros on 8/30/17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface WellDelegate : NSImageView {
    
    NSString *urlToPass;
    
}

- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender;

- (NSDragOperation)performDragOperation:(id<NSDraggingInfo>)sender;

- (NSDragOperation)concludeDragOperation:(id<NSDraggingInfo>)sender;

@end
