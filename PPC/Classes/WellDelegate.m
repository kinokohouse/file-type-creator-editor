//
//  WellDelegate.m
//  File Type & Creator Editor
//
//  Created by Petros on 8/30/17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//

#import "WellDelegate.h"
#import "AppDelegate.h"

@implementation WellDelegate

- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender {
    NSPasteboard *pb = [sender draggingPasteboard];
    NSArray *theFiles = [pb propertyListForType:NSFilenamesPboardType];
    if (theFiles == nil) {
        return NO;
    }
    NSString *firstFile = [theFiles objectAtIndex:0];
    NSWorkspace *workSpace = [NSWorkspace sharedWorkspace];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL result = [workSpace isFilePackageAtPath:firstFile];
    if (result) {
        return YES;
    }
    BOOL isDir;
    result = [fileManager fileExistsAtPath:firstFile isDirectory:&isDir];
    if (isDir) {
        return NO;
    }
    return YES;
}

- (NSDragOperation)performDragOperation:(id<NSDraggingInfo>)sender {
    NSPasteboard *pb = [sender draggingPasteboard];
    NSArray *theFiles = [pb propertyListForType:NSFilenamesPboardType];
    urlToPass = [[theFiles objectAtIndex:0] retain];
    return YES;
}

- (NSDragOperation)concludeDragOperation:(id<NSDraggingInfo>)sender {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    [appDelegate setPassedURL:urlToPass];
    [urlToPass release];
    [appDelegate setTimerForImageWellDragItem];
    return YES;
}

@end

