//
//  main.m
//  File Type & Creator Editor
//
//  Created by Petros Loukareas on 31-10-17.
//  Copyright __MyCompanyName__ 2017. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
